﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Media_Library.Models
{
    public class Music
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Artist { get; set; }
        public string Genre { get; set; }
        public int Released { get; set; }
        public string Media { get; set; }
    }
}