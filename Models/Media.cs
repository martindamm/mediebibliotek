﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Media_Library.Models
{
    public class Media
    {
        public int Id { get; set; }
        public string MediaType { get; set; }
    }
}