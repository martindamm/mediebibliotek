﻿using Media_Library.DAL;
using Media_Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Media_Library.Controllers
{
    public class HomeController : Controller
    {  
        public ActionResult Index()
        {
            ViewBag.Message = "This page will show the recently added items.";

            return View();
        }

        public ActionResult Movies()
        {
            ViewBag.Message = "Your movies page.";

            return View();
        }

        public ActionResult Books()
        {
            ViewBag.Message = "Your books page.";

            return View();
        }
    }
}