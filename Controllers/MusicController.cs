﻿using System;
using System.Linq;
using System.Web.Mvc;
using Media_Library.DAL;
using System.Net;
using Media_Library.Models;
using System.Data;
using System.Data.Entity.Infrastructure;

namespace Media_Library.Controllers
{
    public class MusicController : Controller
    {
        private MediaContext db = new MediaContext();

        // GET: Music/Music
        public ActionResult Music(string sortOrder)
        {
            ViewBag.ArtistSortParm = String.IsNullOrEmpty(sortOrder) ? "artist_desc" : "";
            ViewBag.TitleSortParm = sortOrder == "title" ? "title_desc" : "title";
            ViewBag.DateSortParm = sortOrder == "date" ? "date_desc" : "date";
            ViewBag.GenreSortParm = sortOrder == "genre" ? "genre_desc" : "genre";        

            var music = from m in db.Music select m;

            switch (sortOrder)
            {
                case "artist_desc":
                    music = music.OrderByDescending(m => m.Artist);
                    break;
                case "title":
                    music = music.OrderBy(m => m.Title);
                    break;
                case "title_desc":
                    music = music.OrderByDescending(m => m.Title);
                    break;
                case "genre":
                    music = music.OrderBy(m => m.Genre);
                    break;
                case "genre_desc":
                    music = music.OrderByDescending(m => m.Genre);
                    break;
                case "date":
                    music = music.OrderBy(m => m.Released);
                    break;
                case "date_desc":
                    music = music.OrderByDescending(m => m.Released);
                    break;
                default: // artist ascending
                    music = music.OrderBy(m => m.Artist);                    
                    break;
            }

            return View(music.ToList());
        }

        // GET: Music/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Music/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Artist, Title, Genre, Released, Media")] Music music)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Music.Add(music);
                    db.SaveChanges();
                    return RedirectToAction("Music");
                }
            }
            catch (DataException /* dex */)
            {
                // Log the error (uncomment dex variable name and add a line here to write a log.)
                ModelState.AddModelError("", "Unable to save changes, try again.");
            }
            return View(music);
        }

        // GET: Music/Edit/1
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Music music = db.Music.Find(id);
            if (music == null)
            {
                return HttpNotFound();
            }
            return View(music);
        }

        // POST: Music/Edit/1
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var musicToUpdate = db.Music.Find(id);
            if (TryUpdateModel(musicToUpdate, "",
                new string[] { "Title", "Artist", "Genre", "Released", "Media" }))
            {
                try
                {
                    db.SaveChanges();

                    return RedirectToAction("Music");
                }
                catch (RetryLimitExceededException /* dex */)
                {
                    // Log the error (uncomment dex variable name and add a line here to write to log.)
                    ModelState.AddModelError("", "Unable to save changes, try again.");
                }
            }
            return View(musicToUpdate);
        }

        // Details
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Music music = db.Music.Find(id);
            if (music == null)
            {
                return HttpNotFound();
            }
            return View(music);
        }

        // GET: Music/Delete/1
        public ActionResult Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Delete failed. Try again, and if the problem persist bang your head into a wall.";
            }
            Music music = db.Music.Find(id);
            if (music == null)
            {
                return HttpNotFound();
            }
            return View(music);
        }

        // POST: Music/Delete/1
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                Music music = db.Music.Find(id);
                db.Music.Remove(music);
                db.SaveChanges();
            }
            catch (RetryLimitExceededException /*dex */)
            {
                // Log the error (uncomment dex variable name and a line here to write to log.)
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Music");
        }

        // Closing open connections using Dispose method
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}