﻿using Media_Library.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Media_Library.DAL
{
    public class MediaContext : DbContext
    {

        public MediaContext() : base("MediaContext")
        {
        }

        //public DbSet<Media> Media { get; set; }
        public DbSet<Music> Music { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}