﻿using System;
using System.Collections.Generic;
using Media_Library.Models;
using System.Linq;

namespace Media_Library.DAL
{
    public class MusicInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<MediaContext>
    {
        protected override void Seed(MediaContext context)
        {
            //var media = new List<Media>
            //{
            //    new Media { MediaType="CD" },
            //    new Media { MediaType="Vinyl" },
            //    new Media { MediaType="DVD" }
            //};
            
            var music = new List<Music>
            {
                new Music { Title="The Link", Artist="Gojira", Genre="Metal", Released=2003, Media="Vinyl"},
                new Music { Title="A Show of Hands", Artist="Rush", Genre="Progressiv rock", Released=1988, Media="Vinyl"},
                new Music { Title="Power Windows", Artist="Rush", Genre="Progressiv rock", Released=1985, Media="Vinyl"},
                new Music { Title="A Farewell To Kings", Artist="Rush", Genre="Progressiv rock", Released=1977, Media="Vinyl"} 
            };

            //media.ForEach(m => context.Media.Add(m));
            music.ForEach(m => context.Music.Add(m));
            context.SaveChanges();
        }
    }
}